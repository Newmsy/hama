import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';

import './App.css';

import AppWrapper from './staticItems/AppWrapper';

const App = () => (
  <Router>
    <div>
      <AppWrapper />
    </div>
  </Router>
);

export default App;
