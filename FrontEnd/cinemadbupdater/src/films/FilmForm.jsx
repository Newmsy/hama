import React from 'react';
import moment from 'moment';

import FilmInputField from './FilmInputField';
import SaveButton from './SaveButton';

export default class FilmForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      filmId: undefined,
      film: {}
    }

    this.updateFilm = this.updateFilm.bind(this);
    this.updateFilmDetail = this.updateFilmDetail.bind(this);

  }

  componentDidMount() {
    const date = moment(new Date()).format("DD-MM-YYYY");
    if (this.props.match) {
      let filmId = this.props.match.match.params.id;
      let film = this.props.films.find(film => film.id === filmId);
      this.setState({
        filmId: this.props.match.match.params.id,
        film
      });
    }
    else {
      this.setState({
        film: {
          id: null,
          title: null,
          synopsis: null,
          'cast': null,
          'directors': null,
          'showingTimes': null,
          'releaseDate': date,
          'filmStatus': 1,
          'img': null
        }
      })
    }
  }

  updateFilm() {
    this.props.updateFilm(this.state.film);
  }

  updateFilmDetail(event) {
    let film = this.state.film;
    let field = (event !== null && event.target) ? event.target.name : null;
    if (field === 'showingTimes') {
      film[field] = (event.target.value).split(',');
    }
    else if (event._d) {
      console.log(event._d);
      let date = moment(event._d, "DD-MM-YYYY");
      film.releaseDate = (event) ? date : null;
    }
    else {
      film[field] = event.target.value;
    }

    this.setState({
      film
    });
  }

  render() {
    return (
      <form>
        {Object.entries(this.state.film).map(([key, value]) => (
          (key !== 'id') ?
            <FilmInputField key={key} field={key} passedValue={value} handleChange={this.updateFilmDetail} />
            : false))}
        <SaveButton handleClick={this.updateFilm} />
      </form>
    );
  }
}
