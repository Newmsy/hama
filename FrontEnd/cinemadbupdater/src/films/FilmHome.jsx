import React from 'react';
import { Switch, Route } from 'react-router-dom';

import FilmsTable from './FilmsTable';
import AddFilm from './AddFilm';
import FilmForm from './FilmForm';

import { urls, filmUrls } from '../js/constants';

export default class FilmHome extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      films: undefined,
    };

    this.headers = new Headers();
    this.headers.append('Accept', 'application/json');
    this.headers.append('Access-Control-Allow-Methods', 'GET');

    this.updateFilm = this.updateFilm.bind(this);
  }

  componentDidMount() {
    let url = `${urls.base1}/${filmUrls.get}`;
    fetch(url)
      .then(response => {
        console.log(response);
        const json = response.json();
        console.log(json);
        return json;
      })
      .then(films => {
        this.setState({
          films,
        });
      })
      .catch(error => console.log(error));
  }

  updateFilm(film) {
    let films = this.state.films;
    if (film.id) {
      let url = `${urls.base1}/${filmUrls.update}`;
      let indexToReplace = films.findIndex(stateFilm => film.id === stateFilm.id);
      films[indexToReplace] = film;
      fetch(url, {
        body: JSON.stringify(film),
        mode: 'cors',
        method: 'POST'
      })
        .then(response => {
          if (response.status === 200) {
            return response.json();
          }
          else {
            throw new Error("Data not saved");
          }
        })
        .then(result => {
          this.setState({
            films
          });
          console.log(result.message);
        });
    }
    else {
      let url = `${urls.base1}/${filmUrls.add}`;
      film.id = parseInt(films[films.length - 1].id) + 1;
      films.push(film);
      fetch(url, {
        body: JSON.stringify(film),
        mode: 'cors',
        method: 'POST'
      })
        .then(response => {
          if (response.status === 200) {
            return response.json();
          }
          else {
            throw new Error("Data not saved");
          }
        })
        .then(result => {
          this.setState({
            films,
          });
          console.log(result.message);
        })
        .catch(result => {
          console.log(`The server rejected the request. Please try again later.`);
        });
    }
  }

  render() {
    return (
      <div>
        <Switch>
          <Route exact path="/filmActions" component={() => <FilmsTable films={this.state.films} />} />
          <Route path="/filmActions/showFilms" component={() => <FilmsTable films={this.state.films} />} />
          <Route path="/filmActions/editFilm/:id" children={(match) => <FilmForm films={this.state.films} updateFilm={this.updateFilm} match={match} />} />
          <Route path="/filmActions/addFilm" children={() => <AddFilm updateFilm={this.updateFilm} />} />
        </Switch>
      </div>
    );
  }
}
