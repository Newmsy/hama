import React from 'react';
import moment from 'moment';
import DatePicker from 'react-datepicker';

import 'react-datepicker/dist/react-datepicker.css';

import FilmStatusSelector from './FilmStatusSelector';


const FilmInputField = (props) => {
  switch (props.field) {
    case 'synopsis':
      return (
        <div>
          <label>{props.field}:</label>
          <textarea name={props.field} rows="5" defaultValue={props.passedValue} onChange={props.handleChange}></textarea>
        </div>
      );
      break;
    case 'filmStatus':
      return (
        <div>
          <label>{props.field}:</label>
          <FilmStatusSelector field={props.field} status={props.passedValue} handleChange={props.handleChange} />
        </div>
      );
      break;
    case 'releaseDate':
      let date = moment(props.passedValue, "DD-MM-YYYY");
      return (
        <div>
          <label>{props.field}:</label>
          <DatePicker
            selected={date}
            dateFormat="DD-MM-YYYY"
            onChange={props.handleChange}
          />
        </div>
      );
      break;
    default:
      return (
        <div>
          <label>{props.field}:</label>
          <input type="text" name={props.field} defaultValue={props.passedValue} onChange={props.handleChange} />
        </div>
      );
      break;
  }
}

export default FilmInputField;
