import React from 'react';

const FilmStatusSelector = props => (
  <select name={props.field} defaultValue={props.status} onChange={props.handleChange}>
    <option value="1">Current</option>
    <option value="2">Future</option>
    <option value="3">Archive</option>
  </select>
);

export default FilmStatusSelector;
