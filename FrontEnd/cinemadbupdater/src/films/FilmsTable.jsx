import React from 'react';

import FilmsTableContentRow from './FilmsTableContentRow';
import FilmsTableHeaderRow from './FilmsTableHeaderRow';

const FilmTable = (props) => {
  return (
  <div>
    <h1>All films</h1>
    <p>Click on a film to edit it</p>
    <table>
      <FilmsTableHeaderRow />
      <tbody>
        {props.films !== undefined ? props.films.map((film) => (
          <FilmsTableContentRow
            key={film.id}
            id={film.id}
            title={film.title}
          />
        )) : false}
      </tbody>
    </table>
  </div>
  );
}

export default FilmTable;
