import React from 'react';
import { Link } from 'react-router-dom';

const FilmsTableContentRow = (props) => (
    <tr>
      <td><Link to={`/filmActions/editFilm/${props.id}`}>{props.title}</Link></td>
    </tr>
);

export default FilmsTableContentRow;
