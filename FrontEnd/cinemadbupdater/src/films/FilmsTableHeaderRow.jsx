import React from 'react';

const FilmsTableHeaderRow = () => (
  <thead>
    <tr>
      <td>Film Title</td>
    </tr>
  </thead>
);

export default FilmsTableHeaderRow;
