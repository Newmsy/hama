import React from 'react';

const SaveButton = (props) => {
  return (
    <button className="button success" type="button" onClick={() => { props.handleClick(); }}>
      Save
    </button>
  );
}

export default SaveButton;
