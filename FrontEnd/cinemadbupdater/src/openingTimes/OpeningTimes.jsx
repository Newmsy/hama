import React from 'react';

import OpeningTimesTable from './OpeningTimesTable';

const OpeningTimes = props => (
  <div className="large-12 small-12 columns">
    <section>
      <header>
        <h2>QA Cinemas Opening Times</h2>
        <p>Edit the times as needed and then click SAVE below</p>
      </header>
      {/* <OpeningTimesTable mode={props.mode} /> */}
      <OpeningTimesTable />
    </section>
  </div>
);

export default OpeningTimes;
