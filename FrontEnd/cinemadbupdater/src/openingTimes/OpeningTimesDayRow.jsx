import React from 'react';

export default class OpeningTimesDayRow extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      open: this.props.open,
      close: this.props.close
    }

    this.handleCellChange = this.handleCellChange.bind(this);
  }

  handleCellChange(event) {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

  render() {
    return (
      <tr>
        <td>{this.props.day}</td>
        <td suppressContentEditableWarning={true} contentEditable onKeyUp={this.handleCellChange} onBlur={this.props.handleChange} id={this.props.day + 'open'}>{this.state.open}</td>
        <td suppressContentEditableWarning={true} contentEditable onKeyUp={this.handleCellChange} onBlur={this.props.handleChange} id={this.props.day + 'close'}>{this.state.close}</td>
      </tr>
    );
  }
}