import React from 'react';

const OpeningTimesHeaderRow = () => (
  <thead>
    <tr>
      <th>Day</th>
      <th>Open</th>
      <th>Close</th>
    </tr>
  </thead>
);

export default OpeningTimesHeaderRow;
