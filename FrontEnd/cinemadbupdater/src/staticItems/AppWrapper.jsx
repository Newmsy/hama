import React, { Component } from 'react';

import Header from './Header';
import Footer from './Footer';
import Content from './Content';

class AppWrapper extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mode: ''
    }

    this.setMode = event => {
      this.setState({
        mode: event.target.value
      });
    };
  }
  render() {
    return (
      <div>
        {/* <Header mode={this.state.mode} setMode={this.setMode} />
        <Content mode={this.state.mode} setMode={this.setMode} /> */}
        <Header />
        <Content />
        <Footer />
      </div>
    );
  }
}

export default AppWrapper;