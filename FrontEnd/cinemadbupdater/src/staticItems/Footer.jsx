import React from 'react';

const Footer = () => (
  <footer>
    <p>&copy;QA Cinemas 2017-</p>
  </footer>
);

export default Footer;
