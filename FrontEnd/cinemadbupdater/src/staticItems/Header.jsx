import React from 'react';

import HeaderNav from './HeaderNav';

const Header = props => (
  <nav className="top-bar">
    <div className="top-bar-title">
      <span data-responsive-toggle="menu">
        <button
          type="button"
          className="menu-icon dark"
          data-toggle=""
        ></button>
      </span>
      <h1 className="header">KPMG Cinemas</h1>
    </div>
    {/* {props.mode ? <HeaderNav /> : null} */}
    <HeaderNav />
  </nav>
);

export default Header;
