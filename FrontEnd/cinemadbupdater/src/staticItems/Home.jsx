import React from 'react';
import { Link } from 'react-router-dom';

const Home = (props) => (
  <div>
    <div className="small-12 medium-12 large-12 columns">
      <h1>Welcome to KPMG Cinemas Database Updater</h1>
      <p>
        Please use the navigation links above or one of the links below to choose what you wish to do.
      </p>
    </div>
    <table className="hover">
      <thead>
        <tr>
          <th>Click an action below</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>
            <Link to="filmActions/showFilms" style={!props.mode ? { pointerEvents: "none" } : null}>
              Show all of the films (and choose one to edit)
            </Link>
          </td>
        </tr>
        <tr>
          <td>
            <Link to="/filmActions/addFilm" style={!props.mode ? { pointerEvents: "none" } : null}>
              Add a new film
            </Link>
          </td>
        </tr>
        <tr>
          <td>
            <Link to="/showOpeningTimes" style={!props.mode ? { pointerEvents: "none" } : null}>
              Show the opening times (and choose a day to edit)
            </Link>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
);

export default Home;
