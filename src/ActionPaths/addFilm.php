<?php
namespace Actions;

include_once dirName(__DIR__) . '/autoload.php';

use Db\Db;
use Mappers\FilmMapper;
use Models\Film;
use Models\Response;

// Response Headers
// Allow requests from any origin
header("Access-Control-Allow-Origin: *");
// Allow only requests that have been made using the POST method
header("Access-Control-Allow-Methods: POST");
// Inform requester that response is in form of json (utf-8 charset)
header("Content-Type: application/json; charset=UTF-8");


// The if statament ensures that the Access-Control-Allow-Methods has been truly adhered to
echo $_SERVER['REQUEST_METHOD'];
if($_SERVER['REQUEST_METHOD'] == 'POST') {
  // instantiate database and product object
  $db = Db::getInstance();

  // initialize mapper object
  $filmsMapper = new FilmMapper($db);

  $data = json_decode(file_get_contents("php://input"));

  foreach($data as $key => $value) {
    if($key === 'showingTimes') {
      $value = implode(",", $value);
      $data->$key = $value;
    }
  }

  // initialize film object
  $film = new Film($data);

  $result = $filmsMapper->addFilm($film);

  if($result == $film->id) {
    echo json_encode(new Response(1, "Data Saved"));
  }
  else {
    echo json_encode(new Response(0, "Something went wrong, data not saved"));
  }
}
else {
  echo json_encode(new Response(0, "The server rejected the request"));
}
