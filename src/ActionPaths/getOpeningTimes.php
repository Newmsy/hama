<?php
namespace Restful\Actions;

include_once dirName(__DIR__) . '/autoload.php';


use Db\Db;
use Mappers\OpeningTimesMapper;
use Models\Response;

// Response Headers
// Allow requests from any origin
header("Access-Control-Allow-Origin: *");
// Allow only requests that have been made using the POST method
header("Access-Control-Allow-Methods: GET");
// Inform requester that response is in form of json (utf-8 charset)
header("Content-Type: application/json; charset=UTF-8");

// The if statament ensures that the Access-Control-Allow-Methods has been truly adhered to
if($_SERVER['REQUEST_METHOD'] == 'GET') {
  // instantiate database and product object
  $db = Db::getInstance();

  // initialize object
  $openingTimesMapper = new OpeningTimesMapper($db);

  $openingTimes = $openingTimesMapper->getOpeningTimes();

  $num = count($openingTimes);

  if($num > 0) {
    echo json_encode($openingTimes);
  }
  else {
    echo json_encode(new Response(0, "No opening times found."));
  }
}
else {
  echo json_encode(new Response(0, "The server rejected the request"));
}
