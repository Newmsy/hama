<?php

namespace Actions;

include_once dirName(__DIR__) . '/autoload.php';

use Db\Db;
use Mappers\FilmMapper;
use Models\Film;
use Models\Response;

// Response Headers
// Allow requests from any origin
header("Access-Control-Allow-Origin: *");
// Allow only requests that have been made using the POST method
header("Access-Control-Allow-Methods: POST");
// Inform requester that response is in form of json (utf-8 charset)
header("Content-Type: application/json; charset=UTF-8");


// The if statament ensures that the Access-Control-Allow-Methods has been truly adhered to
if($_SERVER['REQUEST_METHOD'] == 'POST') {
  // instantiate database and product object
  $db = Db::getInstance();

  // initialize mapper object
  $filmsMapper = new FilmMapper($db);

  // Set up a null object to create a new Film instance
  $nullFilm = array(
    'id'=>null,
    'title'=>null,
    'synopsis'=>null,
    'cast'=>null,
    'directors'=>null,
    'showingTimes'=>null,
    'releaseDate'=>null,
    'filmStatus'=>null,
    'img'=>null
  );

  // Set the filters for sanitising
  $filters = array(
    'id'=>FILTER_SANITIZE_NUMBER_INT,
    'title'=>FILTER_SANITIZE_STRING,
    'synopsis'=>FILTER_SANITIZE_STRING,
    'cast'=>FILTER_SANITIZE_STRING,
    'directors'=>FILTER_SANITIZE_STRING,
    'showingTimes'=>FILTER_SANITIZE_STRING,
    'releaseDate'=>FILTER_SANITIZE_STRING,
    'filmStatus'=>FILTER_SANITIZE_NUMBER_INT,
    'img'=>FILTER_SANITIZE_STRING
  );

  // Create a new film object with nulls
  $filteredFilm = new Film($nullFilm);

  // Collect the data sent fromt the front end into an object
  $data = json_decode(file_get_contents("php://input"));

  // Set the value of each key in the Film object after sanitizing
  foreach($data as $key=>$value) {
    $filteredFilm->$key = filter_var($value, $filters[$key]);
  }

  // Call the query to update the record in the database
  $result = $filmsMapper->updateFilm($filteredFilm);

  // Return the response
  if($result == 1) {
    echo json_encode(new Response(1, "Data Saved"));
  }
  else {
    echo json_encode(new Response(0, "Something went wrong, data not saved"));
  }
}
else {
  echo json_encode(new Response(0, "The server rejected the request"));
}