<?php
namespace Db;

use PDO, PDOException;

class Db {

  private static $instance = NULL;
  private static $dsn = "mysql:dbname=qacinemas;host=localhost;charset=utf8";
  private static $user = 'root';
  private static $password = '';

  private function __construct() {}

  private function __clone() {}

  public function getInstance() {

    if(!isset(self::$instance)) {

      $pdoOptions[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;

      try {
        self::$instance = new PDO(self::$dsn, self::$user, self::$password, $pdoOptions);
      }
      catch(PDOException $e) {
        $message = $e->getMessage();
        echo $message;
        exit();
      }
    }

    return self::$instance;
  }

  public static function tearDown() {

    static::$instance = NULL;
  }
}
