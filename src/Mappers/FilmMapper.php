<?php
namespace Mappers;

use Models\Film;
use PDO, PDOException;

class FilmMapper {

  protected $conn;
  protected $tableName = 'films';

  public function __construct($dbConnection) {
    $this->conn = $dbConnection;
  }

  public function getFilms() {

    $sqlQuery = "SELECT * FROM $this->tableName";
    $stmt = $this->conn->prepare($sqlQuery);
    $stmt->execute();

    $films = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $numberOfFilms = count($films);

    if($numberOfFilms > 0) {
      return $films;
    }
  }

  public function getFilm($id) {
  
    $sqlQuery = "SELECT * FROM $this->tableName WHERE id=:id";
    $stmt = $this->conn->prepare($sqlQuery);
    $stmt->bindParam(':id', $id);
  
    $stmt->execute();
  
    $films = $stmt->fetchAll(PDO::FETCH_ASSOC);
  
    $numberOfFilms = count($films);
  
    if($numberOfFilms === 1) {
      return $films;
    }
    else {
      return array(
        "message1" => "There was an error with the request",
        "message2" => "It returned no films or more than 1 film"
      );
    }
  }

  public function updateFilm($updatedFilmObject) {

    $sqlQuery = "UPDATE $this->tableName SET title=:title, synopsis=:synopsis, cast=:cast, directors=:directors,
    showingTimes=:showingTimes, releaseDate=:releaseDate, filmStatus=:filmStatus, img=:img WHERE id=:id";

    $stmt = $this->conn->prepare($sqlQuery);
    $stmt->bindParam(':title', $updatedFilmObject->title);
    $stmt->bindParam(':synopsis', $updatedFilmObject->synopsis);
    $stmt->bindParam(':cast', $updatedFilmObject->cast);
    $stmt->bindParam(':directors', $updatedFilmObject->directors);
    $stmt->bindParam(':showingTimes', $updatedFilmObject->showingTimes);
    $stmt->bindParam(':releaseDate', $updatedFilmObject->releaseDate);
    $stmt->bindParam(':filmStatus', $updatedFilmObject->filmStatus);
    $stmt->bindParam(':img', $updatedFilmObject->img);
    $stmt->bindParam(':id', $updatedFilmObject->id);

    return $stmt->execute();

  }

  public function addFilm($newFilmObject) {

    $sqlQuery = "INSERT INTO $this->tableName(title, synopsis, cast, directors,
      showingTimes, releaseDate, filmStatus, img)
      VALUES (:title, :synopsis, :cast, :directors, :showingTimes, :releaseDate, :filmStatus, :img)";

    $stmt = $this->conn->prepare($sqlQuery);
    $stmt->bindParam(':title', $newFilmObject->title);
    $stmt->bindParam(':synopsis', $newFilmObject->synopsis);
    $stmt->bindParam(':cast', $newFilmObject->cast);
    $stmt->bindParam(':directors', $newFilmObject->directors);
    $stmt->bindParam(':showingTimes', $newFilmObject->showingTimes);
    $stmt->bindParam(':releaseDate', $newFilmObject->releaseDate);
    $stmt->bindParam(':filmStatus', $newFilmObject->filmStatus);
    $stmt->bindParam(':img', $newFilmObject->img);

    $stmt->execute();

    return $this->conn->lastInsertId();
  }
}
