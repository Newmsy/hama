<?php
namespace Models;

class Film {

  public $id;
  public $title;
  public $synopsis;
  public $cast;
  public $directors;
  public $showingTimes;
  public $releaseDate;
  public $filmStatus;
  public $img;

  public function __construct($data) {

    foreach($data as $key => $val) {
            if(property_exists(__CLASS__,$key)) {
                $this->$key = $val;
            }
        }
  }
}
