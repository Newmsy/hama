<?php

namespace Models;

class OpeningTimes {

  public $id;
  public $day;
  public $open;
  public $close;

  public function __construct($data) {

    foreach($data as $key => $val) {
            if(property_exists(__CLASS__,$key)) {
                $this->$key = $val;
            }
        }
  }
}
