<?php

namespace Models;

class Response {

  public $id;
  public $message;

  public function __construct($id, $message) {
    $this->id = $id;
    $this->message = $message;
  }

}
