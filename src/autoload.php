<?php
  spl_autoload_register(function ($class) {
    // echo 'Autoloading classes...';
    // echo $class . "\n";
    $class = str_replace("\\", DIRECTORY_SEPARATOR, $class);
    // echo __DIR__ . DIRECTORY_SEPARATOR . $class . "\n";


    if(file_exists(__DIR__ . DIRECTORY_SEPARATOR . $class . '.php')) {
      // echo "Found: " . __DIR__ . DIRECTORY_SEPARATOR . $class . "\n";
      require_once(__DIR__ . DIRECTORY_SEPARATOR . $class . '.php');
      return;
    }
  });
