<?php
namespace Tests\DbOperations;
use Db\Db;

abstract class DatabaseTestCase extends \PHPUnit\DbUnit\TestCase {

  // Only instantiate adapter once per test
  // static private $adapter = null;
  // Only instantiate pdo once for test clean-up/fixture load
  static private $pdo = null;
  // Only instantiate PHPUnit_Extensions_Database_DB_IDatabaseConnection once per test
  private $conn = null;

  final public function getConnection() {
    Db::tearDown();
    if($this->conn === null) {
      if(self::$pdo === null) {
        self::$pdo = Db::getInstance();
      }
      $this->conn = $this->createDefaultDBConnection(self::$pdo);
    }
    return $this->conn;
  }
}
