<?php
namespace Tests\Mappers;

use Tests\DbOperations\DatabaseTestCase;
use Tests\DbOperations\ArrayDataSet;

use Mappers\FilmMapper;
use Models\Film;
use Db\Db;

class FilmTest extends DatabaseTestCase {

  public function getDataSet() {

    return $this->createFlatXMLDataSet(__DIR__.'/../../Fixtures/film-test.xml');
  }

  public function testGetAllFilmsHasThreeResults() {
    $filmMapper = new FilmMapper(Db::getInstance());

    $films = $filmMapper->getFilms();

    $numberOfFilms = count($films);

    $testResult =  $this->getConnection()->getRowCount('films');

    $this->assertEquals($testResult, $numberOfFilms);
  }

  public function testGetAllFilmsResults() {

    $filmMapper = new FilmMapper(Db::getInstance());

    $films = $filmMapper->getFilms();

    $testTable = $this->getConnection()->createQueryTable('films', 'SELECT * FROM films');

    $rowCounter = 0;

    foreach($films as $film) {

      $testRow = $testTable->getRow($rowCounter++);

      $testResult = ($film === $testRow);

      $this->assertTrue($testResult);
    }
  }

  public function testGetFilmReturnsExactly1Result() {
    $filmMapper = new FilmMapper(Db::getInstance());

    $films = $filmMapper->getFilm('1');

    $numberOfFilms = count($films);

    $testTable = $this->getConnection()->createQueryTable('films', 'SELECT * FROM films WHERE id=1');

    $testResult = $testTable->getRowCount();

    $this->assertTrue($testResult === $numberOfFilms);
  }

  public function testGetFilmReturnsCorrectFilm() {
    $filmMapper = new FilmMapper(Db::getInstance());

    $films = $filmMapper->getFilm(1);

    foreach($films as $filmResult) {
        $film = $filmResult;
    }

    $testTable = $this->getConnection()->createQueryTable('films', 'SELECT * FROM films WHERE id=1');

    $testRow = $testTable->getRow(0);

    $testResult = ($film === $testRow);

    $this->assertTrue($testResult);
  }

  public function testUpdateFilm() {

    $filmMapper = new FilmMapper(Db::getInstance());

    // Updated film array
    $updatedFilm = new Film(array(
      'id' => 8,
      'title' => "New film title",
      'synopsis' => "New film synopsis",
      'cast' => "Film cast",
      'directors' => "Film directors",
      'showingTimes' => "New showing times",
      'releaseDate' => "2018-03-03",
      'filmStatus' => 1,
      'img' => "images/newFilmImage.jpg"
    ));

    $filmUpdated = $filmMapper->updateFilm($updatedFilm);

    $this->assertTrue($filmUpdated);

    // Run check on updated film
    if($filmUpdated) {

      $updatedResults = $filmMapper->getFilm($updatedFilm->id);

      foreach($updatedResults as $film) {
        $updatedResult = $film;
      }

      $id = $updatedFilm->id;

      $testTable = $this->getConnection()->createQueryTable('films', "SELECT * FROM films WHERE id=$id");

      $testRow = $testTable->getRow(0);

      $testResult = ($updatedResult === $testRow);

      $this->assertTrue($testResult);
    }
  }

  public function testAddFilmIncreasesCountOfGetFilms() {

    $filmMapper = new FilmMapper(Db::getInstance());

    $newFilm = new Film(array(
      'title' => "New film title",
      'synopsis' => "New film synopsis",
      'cast' => "Film cast",
      'directors' => "Film directors",
      'showingTimes' => "New showing times",
      'releaseDate' => "2018-03-03",
      'filmStatus' => 1,
      'img' => "images/newFilmImage.jpg"
    ));

    $addResult = $filmMapper->addFilm($newFilm);

    $result = ($addResult !== 0);

    $this->assertTrue($result);

    if($result) {
      $films = $filmMapper->getFilms();

      $numberOfFilms = count($films);

      $testResult = $this->getConnection()->getRowCount('films');

      $this->assertEquals($testResult, $numberOfFilms);
    }
  }

  public function testAddFilmDetailsAreCorrect() {
    
    $filmMapper = new FilmMapper(Db::getInstance());

    $newFilm = new Film(array(
      'title' => "New film title",
      'synopsis' => "New film synopsis",
      'cast' => "Film cast",
      'directors' => "Film directors",
      'showingTimes' => "New showing times",
      'releaseDate' => "2018-03-03",
      'filmStatus' => 1,
      'img' => "images/newFilmImage.jpg"
    ));

    $addResult = $filmMapper->addFilm($newFilm);

    if($addResult) {

      $filmsAdded = $filmMapper->getFilm($addResult);

      foreach($filmsAdded as $film) {
        $filmAdded = $film;
      }

      $testTable = $this->getConnection()->createQueryTable('films', "SELECT * FROM films WHERE id=$addResult");

      $testRow = $testTable->getRow(0);

      $testResult = ($testRow === $filmAdded);

      $this->assertTrue($testResult);
    }
  }
}
