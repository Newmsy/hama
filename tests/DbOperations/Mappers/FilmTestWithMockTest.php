<?php
namespace Tests\Mappers;

use Models\Film;

// use Tests\DbOperations\DatabaseTestCase;
// use Tests\DbOperations\ArrayDataSet;

//require_once('../../bootstrap.php');

class FilmTestWithMockTest extends \PHPUnit\Framework\TestCase
{
    public function testGetAllFilmsHasThreeResults()
    {
        $filmsArray = array(
            new Film(array("Film 1", "Title 1", "Synopsis 1", "Cast 1", "Directors 1", "Show Times 1", "Release Date 1", "Film Status 1", "Image 1")),
            new Film(array("Film 2", "Title 2", "Synopsis 2", "Cast 2", "Directors 2", "Show Times 2", "Release Date 2", "Film Status 2", "Image 2")),
            new Film(array("Film 3", "Title 3", "Synopsis 3", "Cast 3", "Directors 3", "Show Times 3", "Release Date 3", "Film Status 3", "Image 3"))
        );

        $filmMapper = $this->getMockBuilder(FilmMapper::class)
            ->setMethods(['getFilms'])
            ->getMock();

        $filmMapper->expects($this->once())
            ->method('getFilms')
            ->will($this->returnValue($filmsArray));

        $films = $filmMapper->getFilms();

        $numberOfFilms = count($films);

        $this->assertEquals(3, $numberOfFilms);
    }
}
